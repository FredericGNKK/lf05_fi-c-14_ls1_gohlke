package Konsolenausgabe;

public class Temperaturtabelle {
	public static void main(String[] args) {
		int fahrenheit1 = -20;
		int fahrenheit2 = -10;
		int fahrenheit3 = 0;
		int fahrenheit4 = 20;
		int fahrenheit5 = 30;
		
		double celsius1 = -28.8889;
		double celsius2 = -23.3333;
		double celsius3 = -17.7778;
		double celsius4 = -6.6667;
		double celsius5 = -1.1111;
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s", "Celsius");
		System.out.println();
		System.out.println("-----------------------");
		System.out.printf("%-12d", fahrenheit1);
		System.out.print("|");
		System.out.printf("%10.2f", celsius1);
		System.out.println();
		System.out.printf("%-12d", fahrenheit2);
		System.out.print("|");
		System.out.printf("%10.2f", celsius2);
		System.out.println();
		System.out.printf("%-12d", fahrenheit3);
		System.out.print("|");
		System.out.printf("%10.2f", celsius3);
		System.out.println();
		System.out.printf("%-12d", fahrenheit4);
		System.out.print("|");
		System.out.printf("%10.2f", celsius4);
		System.out.println();
		System.out.printf("%-12d", fahrenheit5);
		System.out.print("|");
		System.out.printf("%10.2f", celsius5);
		System.out.println();
		
		
		
	}
}
