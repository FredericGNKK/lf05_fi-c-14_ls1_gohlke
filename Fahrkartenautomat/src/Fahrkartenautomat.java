﻿import java.util.InputMismatchException;
import java.util.Scanner;

class Fahrkartenautomat
{	
	private static Scanner tastatur = new Scanner(System.in);
	private static int beenden = 1;
	
	public static int userEingabe(){
		
		int userEingabe = -1;		
		try{
			userEingabe = tastatur.nextInt();			
		}
		catch(InputMismatchException e) {
			System.out.println("Eingabe ungültig! Bitte wiederholen Sie ihre Eingabe!");
			tastatur.nextLine();
		}		
		return userEingabe;
	}
	
	public static void ticketmenü(String[] tickets, double[] preise) {
		System.out.print("Fahrkartenbestellvorgang:");
		System.out.println();
		for(int i = 0; i < 25; i++) {
			System.out.print("=");
		}
		for(int i = 0; i < 2; i++) {
			System.out.println();
		}
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin ABC aus:");
		for(int i = 0; i < tickets.length; i++) {
			String preiseAlsstring = String.format("%.2f", preise[i]);
			System.out.println("	" + tickets[i] + " [" + preiseAlsstring + " EUR] (" + (i+1) + ")");
		}		
		System.out.println("	Bezahlen (99)");
		System.out.println("	Beenden (100)");
		System.out.println();
	}

	public static double fahrkartenbestellungErfassen() 
	{
		double ticketpreis = 0;
		int anzahlTickets = 0;
		boolean eingabeüberprüfungPreis = false;
		boolean eingabeüberprüfungAnzahl = false;
		int userEingabeTicket = -2;
		int userEingabeAnzahl = -2;
		double zwischensumme = 0.0;
		String[] tickets = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
							"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
							"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
							"Kleingruppen-Tageskarte Berlin ABC"};
		double[] preise = {2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9};
		
		

		do {
			ticketpreis = 0;
			anzahlTickets = 0;
			eingabeüberprüfungPreis = false;
			eingabeüberprüfungAnzahl = false;
			userEingabeTicket = -2;
			userEingabeAnzahl = -2;			
			ticketmenü(tickets, preise);

			do {		
				System.out.print("Ihre Wahl:");			

				userEingabeTicket = userEingabe();			
				while(userEingabeTicket == -1) {
					System.out.print("Ihre Wahl:");
					userEingabeTicket = userEingabe();				
				}
				if(userEingabeTicket > 0 && userEingabeTicket <= tickets.length || 
				   userEingabeTicket == 99  || userEingabeTicket == 100) {
					
					if(userEingabeTicket == 99) {
						if(zwischensumme > 0) {
							eingabeüberprüfungPreis = true;
						}
						else {
							System.out.println("Bitte wählen Sie zuerst ein Ticket aus!");
						}
					}
					else if(userEingabeTicket == 100) {						
						beenden();
						eingabeüberprüfungPreis = true;
					}
					else {
						ticketpreis = preise[(userEingabeTicket-1)];
						eingabeüberprüfungPreis = true;
					}
				}
				else {
					System.out.println("Falsche Eingabe! Bitte wiederholen Sie Ihre Eingabe!");
				}
			}while(!eingabeüberprüfungPreis);						

			while(!eingabeüberprüfungAnzahl && userEingabeTicket != 99 && userEingabeTicket != 100){
							

				System.out.print("Anzahl der Tickets:");
				userEingabeAnzahl = userEingabe();	

				while(userEingabeAnzahl == -1) {
					System.out.print("Anzahl der Tickets:");
					System.out.println(userEingabeAnzahl);
					userEingabeAnzahl = userEingabe();				
				}			
				if(userEingabeAnzahl >= 0) {
					anzahlTickets = userEingabeAnzahl;
					eingabeüberprüfungAnzahl = true;
				}
				else {
					System.out.println("Falsche Eingabe! Bitte wiederholen Sie Ihre Eingabe!");
				}
			}
			zwischensumme += anzahlTickets * ticketpreis;
			if(userEingabeTicket != 99 && userEingabeTicket != 100) {
				String zwischensummeAlsString = String.format("%.2f", zwischensumme);
				System.out.println();
				System.out.println("Zwischensumme: " + zwischensummeAlsString + " €");
				System.out.println();
			}			
		}while(userEingabeTicket != 99 && userEingabeTicket != 100);


		double zuZahlenderBetrag = zwischensumme;
		return zuZahlenderBetrag;	    
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) 
	{		
		double eingezahlterGesamtbetrag = 0.00;
		double nochZuZahlenderBetrag =  zuZahlenderBetrag - eingezahlterGesamtbetrag;	       
		double eingeworfeneMünze;

		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{    
			String nochZuZahlenderBetragAlsString = String.format("%.2f", nochZuZahlenderBetrag);
			System.out.println("Noch zu zahlen: " + nochZuZahlenderBetragAlsString + " Euro");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			if(eingeworfeneMünze == 0.05 || eingeworfeneMünze == 0.1 || eingeworfeneMünze == 0.2 ||
					eingeworfeneMünze == 0.5 || eingeworfeneMünze == 1 || eingeworfeneMünze == 2) {
				eingezahlterGesamtbetrag += eingeworfeneMünze;
				nochZuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			}
			else {
				System.out.println("Eingabe ist ungültig! Bitte versuchen Sie es mit ein anderen Münze nochmal!");
			}

		}		
		return eingezahlterGesamtbetrag-zuZahlenderBetrag;
	}

	public static void warte(int millisekunden) 
	{
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void münzeAusgeben(int betrag, String einheit) 
	{
		System.out.println(betrag + " " + einheit);
	}

	public static void fahrkartenAusgeben()
	{
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rückgabebetrag) 
	{
		if(rückgabebetrag > 0.0)
		{
			String rückgabebetragAlsString = String.format("%.2f", rückgabebetrag);
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetragAlsString + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{				
				münzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{				
				münzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{				
				münzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{				
				münzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{				
				münzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{				
				münzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
				"vor Fahrtantritt entwerten zu lassen!\n"+
				"Wir wünschen Ihnen eine gute Fahrt.");
		System.out.println();
	}
	
	public static void beenden() {
		beenden = 0;
	}

	public static void main(String[] args)
	{
		while(beenden != 0) {
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			if (beenden != 0) {
				double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
				fahrkartenAusgeben();
				rueckgeldAusgeben(rückgabebetrag);
			}
		}	
		tastatur.close();
	}    
}