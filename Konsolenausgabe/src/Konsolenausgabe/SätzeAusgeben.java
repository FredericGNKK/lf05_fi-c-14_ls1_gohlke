package Konsolenausgabe;

public class S�tzeAusgeben {
	public static void main(String[] args) {
		
		//Schreibt in der ganzen Zeile. Die n�chsten Konsolenausgabe w�rde in der n�chsten Zeile ausgegeben werden.
		System.out.println("DaS iSt EiN sAtZ.");
		
		//Schreibt in der Zeile, aber man kann mit der n�chsten Konsolenausgabe trotzdem weiterhin in der gleichen Zeile schreiben, solange sie nicht voll ist.
		System.out.println("dAs IsT kEiN sAtZ!");
	}
}
