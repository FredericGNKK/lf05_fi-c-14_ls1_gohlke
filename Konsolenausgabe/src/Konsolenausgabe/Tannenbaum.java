package Konsolenausgabe;

public class Tannenbaum {

	public static void zeichneLeerzeichen(int anzahlLeerzeichen) {
		for(int i = 0; i < anzahlLeerzeichen; i++)
		{
			System.out.print(" ");
		}
	}

	public static void zeichneSterne(int anzahlSterne) {
		for(int i = 0; i < anzahlSterne; i++)
		{
			System.out.print("*");
		}				
	}
	public static void zeichneBaumstamm(int anzahlLeerzeichen) 
	{
			zeichneLeerzeichen(anzahlLeerzeichen);
			zeichneSterne(3);
			System.out.println();
			zeichneLeerzeichen(anzahlLeerzeichen);
			zeichneSterne(3);
	}
	public static void zeichneTannenbaum(int anzahlSterne, int anzahlLeerzeichen) {
		if(anzahlLeerzeichen > 0) {
			zeichneLeerzeichen(anzahlLeerzeichen);
			zeichneSterne(anzahlSterne);
			System.out.println();	
			zeichneTannenbaum(anzahlSterne + 2, anzahlLeerzeichen - 1);
		}
	}
	public static void main(String[] args) {
		int anzahlLeerzeichen = 22;
		int anzahlSterne = 1;
		zeichneTannenbaum(anzahlSterne, anzahlLeerzeichen);
		zeichneBaumstamm(anzahlLeerzeichen - 1);
	}
}
