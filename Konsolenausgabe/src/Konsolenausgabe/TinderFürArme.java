package Konsolenausgabe;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class TinderF�rArme {	
	public static String readName() {
		String name = "Dieser String ist leer :P";
		boolean tryAgain = true;		
		
		System.out.println("Hallo User!");
		while (tryAgain)
		{
			try
			{
				Scanner myScanner1 = new Scanner(System.in);
				System.out.println("Wie hei�t du?");
				name = myScanner1.next("[\\p{L}]+");				
				tryAgain = false;
			}
			catch (InputMismatchException e)
			{
				System.out.println("Es sind NUR Buchstaben erlaubt!");
				tryAgain = true;
				
			}
		}		
		return name;
	}
	
	
	
	
	public static int readAlter() {
		int alter = -999991;
		boolean tryAgain = true;		

		while (tryAgain)
		{
			try
			{			
				Scanner myScanner2 = new Scanner(System.in);
				System.out.println("Wie alt bist du?");
				alter = myScanner2.nextInt();				
				tryAgain = false;
			}
			catch (InputMismatchException e)
			{
				System.out.println("Es sind NUR Zahlen erlaubt!");
				tryAgain = true;
				
			}			
		}
	//	myScanner2.close();
		return alter;
	}
	public static void main(String[] args) {		
		String name = readName();
		int alter = readAlter();		
		System.out.println("Hallo " + name + "! Sch�n dich kennenzulernen! Ich sehe du bist " + alter + " Jahre alt!");
		
	}
}
