package Konsolenausgabe;

public class Konfiguration_Chaos{
	 
	public static void main(String[] args) {
		final int PRUEFNR = 4;
		double maximum = 100.00;
		double patrone = 46.24;		
		double fuellstand;
		fuellstand = maximum - patrone;
		String bezeichnung = "Q2021_FAB_A";
		String typ = "Automat AVR";
		String name;
		name = typ + " " + bezeichnung;
		char sprachModul = 'd';		
		int cent;
		int euro;
		int summe;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		summe = muenzenCent + muenzenEuro * 100;
		cent = summe % 100;
		euro = summe / 100;
		boolean statusCheck;		
		statusCheck = (euro <= 150) 
		&& (euro >= 50)
		&& (cent != 0)
	    && (sprachModul == 'd')
		&& (fuellstand >= 50.00) 
		&&  (!(PRUEFNR == 5 || PRUEFNR == 6));		
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Prüfnummer : " + PRUEFNR);
		System.out.println("Füllstand Patrone: " + fuellstand + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + statusCheck);		
	}	
}
